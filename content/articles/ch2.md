Title: 2025-09-15
Status: DRAFT
Summary: Progress?

# 2025-09-15: EERC Room 801

A shower of cerulean sparks spewed from a scorched electrical panel, apparently not for the first or even perhaps the tenth time. The panel, seemingly held together solely by a sheer force of stubborn will and zip ties at this point, quickly got yet another dousing from a beleaguered Patricia, holding a refillable multi-class fire extinguisher.

The diminutive violet-haired woman swore profusely under her breath as the last embers succumbed to the foam.

"Your mothers be laid slick on a slab! Gorram couplers can't fucking handle the damned currents! Now, we're going to try this thing again, this time with *feeling*, and you'll work or I swear I'll smash every last blasted one of you into diamonds!"

She tucked an errant stray lock of exuberant hair behind one metal-laden ear, pushed her violet-rimmed glasses back up to the bridge of her nose, and began the test sequence again.

"...does she think the couplers can hear her?" Kiara asked, *sotto voce*.

"At this point, *I* think the couplers can hear her." Genevieve stretched and yawned. "Gods though, I think this is the last test for me. **Some** of us have classes in the morning, and it is well past the time I turn into a pumpkin."

"...can we make pie of you?" Katie quipped, entering the lab in a cloud of cardigan, skirt, and boisterous curly hair. "I could use something warm in my belly, it is godsawful out there today."

"Oh? I have not been outside in...fuck. What day is it?" Kiara searched the room for a clock or calendar, realized none of them were working at all, and turned to Katie again.

"Monday, love, around 07:15." Pulling off her cardigan and placing it on one of the hooks in the wall, Katie looked over the debris-laden tables, workbenches, ping-pong table, and floor, and sighed. "Love, how long have you three *been* here?"

Genevieve raised an eyebrow, "Well, **I** got here around midnight on Saturday, though I did go out and get the food. They were pretty settled already at that point, and each put away around 7,000 calories once they did not have to leave the lab." Chuckling, she idly scratched the back of her head along the shave, continuing, "If I had to guess, probably immediately after their last classes on Friday."

Kiara blushed and mumbled, "...Wednesday..."

Katie enveloped the taller woman in her arms, feeling her sag into the embrace with fatigue. "Love. You need to sleep. I will march you out of here into the blizzard if I have to."

"OH FUCK YEAH! Yo yo yo! Check this shit out!" whooped Patricia. Her face, flushed with exertion and filthy with grease and soot, beamed with pride as a screen filled with numbers beside her on what used to be a ping-pong table, long since converted into a work surface for the lab.

Kiara and Genevieve collided in their simultaneous efforts to traverse the room. Kiara paused and waved Genevieve through the narrow gap, amidst the strewn tools and failed components. "After you."

Genevieve shrugged and stepped gingerly over a tub of miscellaneous bolts and screws, hip-checked two dilapidated Steelcase chairs, and stood beside Patricia, ensuring that the violet-haired woman was between her and the electrical panel at all times.

Kiara attempted the same manuever and immediately went head over heels, having caught her skirt on the bucket. "'s OK. Just the robot puke reached out and grabbed me."

"Yeah, it does that. Just fucking come here already OK?" Patricia was practically bouncing with glee, ready to explode. "**It. Is. WORKING.**"

Kiara composed herself and made it across the obstacle course, squinting at the flowing readings on the screen. She looked over at Genevieve, who nodded and said "She did it. Patricia, you are the queen technowitch of the coven. I bow before your prowess."

The brunette slowly started to smile, filling with joy and hope as the magnitude of the performance became clear. Absent-mindedly, she turned to look into the panel, seeing a pleasing blue glow flickering in regular patterns from the hastily-patched holes and scars in it. Patricia quickly intervenes.

"NO no no no. Not you again. You'll fuck this up right proper. No. You watch the screen; I'll dicker with the couplers, Kay?" The woman drew herself up to the full extent of her 130cm and glared down the woman who had over a head and a half on her. Kiara blinked, steadied herself, and sank wearily into a chair. Then immediately shot back up, having sat upon a pair of dykes. Moving those to the nearest clear-ish spot on a bench, she returned to the chair, crossed her legs, and put her fingers to her temples. She drew a handful of breaths, then looked up, wide-eyed and exuberant at the three other women.

"We *did* it! We successfully hacked the WormCam *and* got it to stabilize at almost two microns diameter! Great work. Patricia, do you think it is safe to leave open for a duration trial?"

"I'll bet my panties on it"

"...ok then. Go ahead and keep the log active, and I am going to..." catching a glare from Katie from across the former ping-pong table, the brunette continued, "...uh. I guess, like, go back home and sleep."

Katie smiled and took the younger woman's hand, gently supporting her as she gathered herself from the chair. "Love, I will take you home. Genevieve will help put this place to rights, *a bit*--" over the glaring protest made from that corner "--and then take herself to sleep."
