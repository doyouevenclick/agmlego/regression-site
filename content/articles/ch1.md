Title: 2025-03-01
Status: DRAFT
Summary: Flashes of inspiration

# 2025-03-01: EERC Room 801

Kiara stood at the whiteboard over the hideous, sort-of-yellow couch, which had likely been old when her father went here. Marker in hand, she was rather lost in thought, distracted by a tangent spurred by the text on the board, hastily and messily written over the last four hours of fast-iteration engineering.

"Katie. Katie. Katie!"

"Yes, love?"

"What if. So. We want to move molecules. And Patrica and Genevieve are nearly done with the upgraded antimatter generator to stabilize the thing. Thing. Um. You know. **Fuck!** The *mouth*, right, so we are nearly there. So. Uh."

Katie smiled, patiently waiting for the other brunette's brain to catch up with her mouth.

"Where was I?"

Katie replied, "Molecules."

"MOLECULES! Right. Yes. Thank you. But we want to move them from the past to now. But. And do not quote me on this, because this is probably all bullshit and I just cannot see it here. But why can we not move molecules from *now* to the--"

"Past! No, I think you are onto something!" Katie ran a hand through her mane of dark, curly hair. "But what would you send back?"

The woman at the board blushed hard, the freckles across her nose vanishing, and her voice fails her partway through something. "...ones..."

"What? What has you so worked up?" Katie leans forward carefully on the ancient four-wheeled disaster that was a 1960s Steelcase office chair. She had had too many pitch-pole moments, kicking the chair across the room to get from desk to desk only to find the wheelbase turning to put two wheels perpendicular to her travel and the chair sending her flying into the heavily-waxed surely-by-now-not-still-asbestos tiled lab floor.

She notices the younger woman has dropped her hands to her waist (and the marker, to the floor), with the tips of her index fingers touching loosely. Behind them, a small bulge just under the hem of her skirt.

Kiara tries again, actually making a sound this time. "Hormones."

Katie suddenly feels a flush herself, and a tightening in points below. "...let me get this straight--"

"As if anything *you* do can be called **straight**--"

"SHUT UP." The woman at the board smiles through her blush. "You mean to say. What if we were to send hormone replacement therapy, back in time, to help late-decision transfolk transition?"

"Well. Um. Yeah, eventually."

"..."

"Uh. So. I was thinking maybe two. In particular. Like. Maybe even just one in particular."

"...gods you are adorable, and sexy, and I love you so much."

Kiara was not sure she *could* blush any harder, but her face was really trying. She dropped her gaze (eye contact, even with an intimate partner, was just so fucking hard) and worked on another phrase, the rest of her thought, and convinced it to actually come out. Her voice strained with the effort, fighting arousal and love and aphasia and anxiety to get all the words out, audible and in the right order.

"You. I want to help *you*. I want to help you get a better result from transition than you would if you started now. Maybe even better than mine."

Katie's face softened, filled with love. Something close to but not exactly like a word spilled from her mouth.

She composed herself, and tried again.

"You are the best thing that has ever happened to me. So. How do we do this?"

The women began to work.
