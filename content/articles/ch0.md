Title: 2027-08-08
Status: DRAFT
Summary: A brief recording

# 2027-08-08: Former Site of Michigan Technological University, Electrical Engineering Resources Center (EERC)

"...this on? Yes! Ok. Technology is hard. Apparently you need to actually *push* the record button--"

"Love. You need to tell the story."

"Right! Right. Sorry. Story. Telling. Yes. So! Um. *Regression* review log, 2027 August 7th--wait, no, 8th. This is the first review of the *Regression* project, which has been deemed a, uh...fuck."

"Love. It is OK. No need to cry. You did your best."

"...sorry. Shit. Ok. The *Regression* project was intended to be an exploration of the new possibilities available in the WormCam from OurWorld. We had a hypothesis that, if we could enlarge the wormhole mouth and stabilize it, we could move not just light but larger matter, perhaps even molecules. The soft goal was to use it to extract living DNA from a passenger pigeon, so we could bring them back from extinction. However, another possibility became apparent early on..."
