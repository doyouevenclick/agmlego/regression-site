Title: Characters
Path-Alias: /characters
Status: PUBLISHED

# Kiara

An often-frazzled woman, roughly 21 years old. Several credits away from a degree in computer engineering, a degree in electrical engineering, a degree in physics...

Kiara started her medical transition without the permission of her folks when she was just 13, forcing the issue. Her folks acquiesced and began her on medically-supervised HRT shortly thereafter.

Kiara is a lightly-freckled brunette, just over 175cm tall and around 63kg, but who is counting?

She wishes she had the graceful ease with people and words that Katie seems to. She also flusters easily, especially when Katie makes wordplay jokes.

# Katie

A powerhouse computer science and software engineering dual major, devops is Katie's truest passion. At 23 years old, she is anxious to finish her degree and find work, hopefully in the local city around the school; no place like the UP to live!

Katie's egg cracked just a couple years ago, and she tries not to be pulled down into regrets at the limited time she has spent trying to transition.

She stands close to 168cm tall and 75kg, with impressively curly dark hair that falls to mid-back.

She often finds herself mothering the others in the coven, especially Kiara.

# Patricia

Boisterous and often in the habit of verbally abusing the technology at her hands to make it work better, no one including Patricia knows just exactly what she is going to school for, or how long she has been doing so. She has a permanent waiver from the dean after a discussion regarding her 13th request for an extension ended in the dean's coffeemaker awaking briefly, convinced that it was the nucleus of the Singularity. Adept in most things electrical and mechanical, she tends to be the one who does the physical hacking on coven projects.

Standing *exactly* 130cm (she will tell you) and 30kg, Patricia generally responds physically to comments like "fun-sized". She brews her own hair dyes, and cycles through them wildly and at random. She tries to match her glasses frames with the bold color _du jour_ She has lost track of the number of piercings she has, but is nearly always game for another.

Patricia is amused at packing as much off-color language into her vernacular as possible while still being understood, especially when she gets to witness the colors it makes Kiara turn.

# Genevieve

13 years ago, Genevieve suggested a correction to her freshman-year physics textbook that ended up published in _Physics_. Since then, she makes herself useful by alternately tying the coven to the ground and whipping together new hypotheses to solve project issues. The school pays her a generous stipend to just hang around campus and "foster innovative thought".

At 203cm and 108kg, Genevieve towers above the others. She blames her height for her perpetual terrible luck with romantic partners, and has largely given up on the concept.

She keeps the left side of her head in an undershave, and generally allows the rest of her vividly orange hair to fall where it will.
