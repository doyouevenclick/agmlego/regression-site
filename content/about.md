Title: About this site
Entry-ID: 7312
UUID: ea27ba28-2630-49b2-a68f-8af76ada9928
Path-Alias: /about
Path-Alias: /contact
Path-Canonical: /about
Path-Alias: /faq
Status: HIDDEN
Disable-Comments: true

Hey there! This is the latest of a string of places I have inhabited over the years. Mostly here for random writings.

### About me

* Initial release: `(time_t)598837320`
* Gender: transgender woman
* Pronouns: she/her
* Favorite colors: mostly blue

By day, I am an estimating engineer (and jill-of-all-trades) at a local industrial-automation division of a major multinational company, but I also like to write, do ill-advised sysadmin stuff, do hacky one-off Python, and sail.

### About this site

A friend recommended I try a mutual's CMS platform,
[Publ](http://publ.beesbuzz.biz). It seems to be working OK. In particular, many of the pages and CSS were partially or wholly copied from [their site templates](https://github.com/PlaidWeb/Publ-templates-beesbuzz.biz).

This site is published through a `salt` deployment triggered from a CI/CD pipeline from a gitlab repo...

### Contact options

If you would like to get in touch with me, here's some choices:

* Email: `agmlego[at]gmail[dot]com`
* Social media: [Mastodon](/mastodon)

