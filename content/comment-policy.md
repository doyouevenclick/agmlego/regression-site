Title: Comment policy
Status: HIDDEN
Path-Alias: /comment-policy
Path-Canonical: /comment-policy
Entry-ID: 4614
UUID: 5cf706b2-9029-41f1-ade4-32d4f70579f6

Thank you for coming to my site! I want to welcome you here. I also want to welcome everyone else here. In order to make this environment as welcoming as possible for everyone, I ask that everyone follow this comment policy.

### Be respectful

Do not bully, harass, or flame. Respect peoples' stated identities, names, and pronouns.

Do not disparage people based on:

* race
* religion
* country of origin
* gender identity (or lack thereof)
* sexual orientation (or lack thereof)
* autistic/allistic/neurodivergent status
* artistic/musical/creative ability
* disabilities (physical, cognitive, or otherwise)

Do not go digging for information on people. Do not use peoples' "dead names" or search for private/deprecated information that they themselves have not intended for people to know at present. "Doxing" is absolutely not tolerated.

This applies to people who are not here, either. Treat every person as if they might be posting a comment. Even if they are someone you hate or are globally reviled. It is possible to criticize without being nasty.

This is not to say you cannot criticize others! Just be sure to criticize based on what they *do* or what they *say*, not based on what or who they *are*.

### Do not spam

Please keep your comments on-topic for the subject at hand. Do not post the same thing repeatedly. Do not use every page as a springboard for advertising your own goods or services. (Posting about things you have *personally* done or made is okay, as long as it is on-topic. But this is not your advertising opportunity.)

Do not post offensive/obscene/pornographic images in comments. Links to such material (if on-topic and not spam) should be clearly marked as such.

Absolutely do not post links to content likely to cause emotional or neurological triggers (including PTSD or epilepsy) without a clear content warning.

### Specific topics to avoid

Unless they are germane to the discussion, please avoid these topics:

* Current US politics
* Comparative superiority/inferiority of established technology choices (operating systems, phones, cars, programming languages, etc.)
  * Note that positive recommendations about a thing are fine! But it is possible to recommend something without disparaging another thing.
* Whether dogs are better than cats (they are not)
* "Concern trolling" about other peoples' decisions
  * Especially regarding identity- or gender-affirming things
  * They have certainly heard it already
  * They are not going to reconsider `$foo` for any `$foo`
  * Yes you think you are so clever in comparing genital surgery with someone cutting off their arms or whatever but we have heard it before and it is just *tiresome*

### What the admin says, goes

These are guidelines, not laws.

If agmlego asks you to stop something, *stop doing it*.
