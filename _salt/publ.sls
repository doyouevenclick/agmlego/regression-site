#!pyobjects

Pkg.installed("memcached")
with User.present("publ"):
    File.directory("/var/lib/publ", user="publ")
    File.directory("/var/run/publ", user="publ")

with File.directory("/usr/share/nginx/frondeur.net"):
    Virtualenv.managed(
        "/usr/share/nginx/frondeur.net/venv",
        requirements="salt://requirements.txt",
        listen_in=[Service("frondeur")],
        python="python3",
    )

    File.recurse(
        "/usr/share/nginx/frondeur.net/app",
        source="salt://_source",
        clean=True,
        user="publ",
        group="www-data",
        listen_in=[Service("frondeur")],
    )

with File.managed(
    "/etc/systemd/system/frondeur.service",
    contents="""
[Unit]
Description=Publ instance for frondeur.net

[Service]
Restart=always
WorkingDirectory=/usr/share/nginx/frondeur.net/app
ExecStart=/usr/share/nginx/frondeur.net/venv/bin/gunicorn -b unix:/var/run/publ/frondeur.socket app:app
ExecReload=/bin/kill -HUP $MAINPID
User=publ

[Install]
WantedBy=default.target
""",
    require=[
        File("/usr/share/nginx/frondeur.net/app"),
        Virtualenv("/usr/share/nginx/frondeur.net/venv"),
        User("publ"),
    ]
):
    Cmd.run("systemctl daemon-reload", watch=[File("/etc/systemd/system/frondeur.service")])
    Service.running("frondeur", enable=True)


Service.running("nginx-frondeur", name="nginx", reload=True)

File.managed(
    "/etc/nginx/sites-enabled/frondeur.net",
    source="salt://nginx.conf",
    listen_in=[
        Service('nginx-frondeur'),
    ],
)
